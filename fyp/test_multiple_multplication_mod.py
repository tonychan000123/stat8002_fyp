from qiskit import QuantumCircuit, QuantumRegister, Aer, execute, ClassicalRegister
from math import ceil, log2
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance
from qiskit.visualization import plot_circuit_layout, plot_histogram
from shor_util import swap, quantum_addition_modified, swap_gate, controlled_multiple_a_mod_N

# Factor 15
N = 15
a = 7
x = 3
num_qubit = ceil(log2(N)) + 1

reg_ctrl = QuantumRegister(1)
reg_x = QuantumRegister(num_qubit)
reg_b = QuantumRegister(num_qubit)
reg_aux = QuantumRegister(1)
creg_b = ClassicalRegister(num_qubit)

qc = QuantumCircuit(reg_ctrl, reg_x, reg_b, reg_aux, creg_b)

qc.x(reg_ctrl)

# Prepare register x
binary_x = bin(x)[2:].zfill(num_qubit)
display(binary_x)
for i in range(len(binary_x)):
    display(binary_x[i])
    if binary_x[i] == '1':
        print(f'inserting {i}')
        qc.x(reg_x[len(binary_x) - 1- i])

# Prepare register b
qc.x(reg_b[0])
swap(qc, reg_b)

controlled_multiple_a_mod_N = controlled_multiple_a_mod_N(a, N, reg_x.size, num_qubit)
qc.append(controlled_multiple_a_mod_N, [reg_ctrl[0], *reg_x._bits, *reg_b._bits, reg_aux[0]])

qc.barrier()
qc.measure(reg_b, creg_b)


backend = Aer.get_backend('qasm_simulator')
quantum_instance = QuantumInstance(backend, shots=1024)
results = execute(qc, backend, shots=2048).result()
counts = results.get_counts()

display(counts)
display(qc.draw())
plot_histogram(counts)