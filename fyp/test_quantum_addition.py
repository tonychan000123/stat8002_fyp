from qiskit import QuantumCircuit, QuantumRegister, Aer, execute
from math import ceil, log2
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance
from qiskit.visualization import plot_circuit_layout, plot_histogram
from shor_util import quantum_addition_modified, swap

# Factor 15
N = 15
num_qubits = ceil(log2(15)) + 1
reg_b = QuantumRegister(num_qubits)
qc = QuantumCircuit(reg_b)

qc.x(reg_b[0])

# Because quantum addition does not invert all qubits, instead we invert the input
# Note that the output of quantum addition is inverted thoughout the circuit
swap(qc, reg_b)

qft = QFT(num_qubits, do_swaps=False)
qc.append(qft, reg_b)

qc.append(quantum_addition_modified(8, num_qubits), reg_b)


qc.append(qft.inverse(), reg_b)

qc.measure_all()

backend = Aer.get_backend('qasm_simulator')
quantum_instance = QuantumInstance(backend, shots=1024)
results = execute(qc, backend, shots=2048).result()
counts = results.get_counts()

display(counts)
display(qc.draw())
plot_histogram(counts)