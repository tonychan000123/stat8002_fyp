from qiskit import QuantumCircuit, QuantumRegister, Aer, execute, ClassicalRegister
from math import ceil, log2
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance
from qiskit.visualization import plot_circuit_layout, plot_histogram
from shor_util import swap, quantum_addition_modified, controlled_unitary_a

# Factor 15
N = 15
a = 7
x = 5
num_qubit = ceil(log2(N)) + 1

reg_ctrl = QuantumRegister(1)
reg_x = QuantumRegister(num_qubit)
reg_b = QuantumRegister(num_qubit)
reg_aux = QuantumRegister(1)
creg_x = ClassicalRegister(num_qubit)

qc = QuantumCircuit(reg_ctrl, reg_x, reg_b, reg_aux, creg_x)

binary_x = bin(x)[2:].zfill(num_qubit)

# Activate ctrl
qc.x(reg_ctrl)

for i in range(len(binary_x)):
    if binary_x[i] == '1':
        qc.x(reg_x[num_qubit - 1- i])

cu_a = controlled_unitary_a(a, N, x)
qc.append(cu_a, [reg_ctrl[0], *reg_x._bits, *reg_b._bits, reg_aux[0]])

qc.barrier()
qc.measure(reg_x, creg_x)

backend = Aer.get_backend('qasm_simulator')
quantum_instance = QuantumInstance(backend, shots=1024)
results = execute(qc, backend, shots=2048).result()
counts = results.get_counts()

display(counts)
display(qc.draw())
plot_histogram(counts)