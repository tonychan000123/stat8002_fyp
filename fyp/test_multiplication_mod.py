from qiskit import QuantumCircuit, QuantumRegister, Aer, execute, ClassicalRegister
from math import ceil, log2
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance
from qiskit.visualization import plot_circuit_layout, plot_histogram
from shor_util import quantum_addition_modified, swap_gate, doubly_controlled_add_a_mod_N


# Factor 15
N = 15
a = 7
num_qubit = ceil(log2(15)) + 1

reg_ctrl = QuantumRegister(2)
reg_b = QuantumRegister(num_qubit)
reg_aux = QuantumRegister(1)
creg_b = ClassicalRegister(num_qubit)

qc = QuantumCircuit(reg_ctrl, reg_b, reg_aux)

qc.x(reg_ctrl)
qc.x(reg_b[0])


swap_gate = swap_gate(num_qubit)
# qc.append(swap_gate, reg_b)

qft = QFT(num_qubit, do_swaps=False)
inverse_qft = qft.inverse()

qc.append(qft, reg_b)

doubly_controlled_add_a_mod_N = doubly_controlled_add_a_mod_N(a, N, num_qubit)
qc.append(doubly_controlled_add_a_mod_N, [*reg_ctrl._bits, *reg_b._bits, *reg_aux._bits])

qc.append(inverse_qft, reg_b)

qc.add_register(creg_b)
qc.measure(reg_b, creg_b)

backend = Aer.get_backend('qasm_simulator')
quantum_instance = QuantumInstance(backend, shots=1024)
results = execute(qc, backend, shots=2048).result()
counts = results.get_counts()

display(counts)
display(qc.draw())
plot_histogram(counts)