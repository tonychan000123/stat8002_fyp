import matplotlib.pyplot as plt

N = 15
a = 9

x_list = range(N)
y_list = [a**x % N for x in x_list]

fig, axis = plt.subplots()

axis.plot(x_list, y_list, 'o-')
fig.suptitle('Period of $a^x$ mod $15$')
axis.set(xlabel = '$x$', ylabel = f'${a}^x$ mod ${N}$')