from qiskit import *
from math import pi
from qiskit.tools.visualization import plot_bloch_multivector

circuit = QuantumCircuit(4, 4)
circuit.x(3)
circuit.p(-2*pi*0/4, 3)

circuit.x(2)
circuit.p(-2*pi*1/5, 2)



simulator = Aer.get_backend('statevector_simulator')

result = execute(circuit, backend = simulator).result()
statevector = result.get_statevector()
print(statevector)

circuit.draw(output='mpl')

plot_bloch_multivector(statevector)
