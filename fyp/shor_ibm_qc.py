from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister, Aer, execute, transpile, assemble
from qiskit.ignis.mitigation.measurement import complete_meas_cal, CompleteMeasFitter
from qiskit.aqua import QuantumInstance
from qiskit.circuit import Gate
from qiskit.circuit.library import QFT
from math import pi, log2, ceil, pow
from qiskit.visualization import plot_histogram
import numpy as np
from shor_util import swap, quantum_addition_modified, controlled_unitary_a


# Factor 15
N = 15
a = 2
x = 1
num_qubit = ceil(log2(N)) + 1

reg_phase = QuantumRegister(2*num_qubit) # Act as control register
reg_x = QuantumRegister(num_qubit)
reg_b = QuantumRegister(num_qubit) # Must be |0>
reg_aux = QuantumRegister(1)

qc = QuantumCircuit(reg_phase, reg_x, reg_b, reg_aux)

qc.x(reg_x[0]) # Set |x> = |1>
qc.h(reg_phase) # QFT of |0>

for i in range(2*num_qubit):
    new_a = int(pow(a, pow(2, i)))

    u_a = controlled_unitary_a(new_a, N, x)
    qc.append(u_a, [reg_phase[i], *reg_x._bits, *reg_b._bits, reg_aux[0]])

inverse_qft = QFT(2*num_qubit, do_swaps=False).inverse()
qc.append(inverse_qft, reg_phase)

qc.barrier()
creg_phase = ClassicalRegister(2*num_qubit)
qc.add_register(creg_phase)
qc.measure(reg_phase, creg_phase)

backend = Aer.get_backend('qasm_simulator')
# quantum_instance = QuantumInstance(backend, shots=1024)
# results = execute(qc, backend, shots=2048).result()
# counts = results.get_counts()

# display(counts)
# display(qc.draw())
# plot_histogram(counts)

cal_circuits, state_labels = complete_meas_cal(qr=reg_phase, circlabel="measerrormitigationcal")

cal_job = execute(cal_circuits, backend = backend, shots = 1024, optimization_level = 0)
cal_results = cal_job.result()
cal_circuits[2].draw()

plot_histogram(cal_results.get_counts(cal_circuits[3]))