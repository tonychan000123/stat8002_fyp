from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister

n = 5
qreg = QuantumRegister(n)
creg = ClassicalRegister(n)
qc = QuantumCircuit(qreg, creg)

for idx, q in enumerate(qreg):
    qc.h(q)

    k = 2
    for i in range(idx+1, len(qreg)):
        print(idx)
        qc.cp(2/2**k, i, q)
        k += 1



qc.draw('mpl')