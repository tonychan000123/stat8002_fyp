from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister, Aer, execute
from qiskit.aqua import QuantumInstance
from qiskit.circuit import Gate
from qiskit.circuit.library import QFT
from math import pi, log2, ceil, pow
from qiskit.visualization import plot_histogram
import numpy as np
from typing import Tuple


def quantum_addition(a: int, num_qubits: int) -> QuantumCircuit:
    
    # Find binary representation of a
    binary_a = bin(a)[2:]
    
    # Append zeros to the binary for circuit implementation, extra zero is added to prevent overflow
    binary_a = binary_a.zfill(num_qubits + 1)

    # Construct a quantum circuit such that the upper half is register for variable a and lower half is register b
    reg_a = QuantumRegister(len(binary_a))
    reg_b = QuantumRegister(num_qubits + 1)
    qc = QuantumCircuit(reg_a, reg_b)

    for i in range(0, num_qubits + 1):
        count = 1
        for j in range(i, num_qubits + 1):
            qc.cp(2*pi/2**count, reg_a[j], reg_b[i])
            count += 1
    
    print(qc.draw())

    print(binary_a)
    return qc.decompose()

def quantum_addition_modified(a: int, num_qubits: int) -> QuantumCircuit:
    """
    Modified version of quantum addition to reduce the number of gate it uses
    """
    # Find binary representation of a
    binary_a = bin(a)[2:]
    
    # Append zeros to the binary for circuit implementation, extra zero is added to prevent overflow
    binary_a = binary_a.zfill(num_qubits)

    # Construct a circuit that only contains register b
    reg_b = QuantumRegister(num_qubits)
    qc = QuantumCircuit(reg_b)

    # Declare phase list to store all phases
    phase_list = np.zeros(num_qubits)

    for i in range(0, num_qubits + 1):
        count = 1
        for j in range(i, num_qubits):
            if binary_a[j] == '1':
                phase_list[i] += 2*pi/2**(count)
            count += 1

    # The number of gates is now smaller or equal to n+1
    for idx, phase in enumerate(phase_list):
        qc.p(phase, idx)
    
    return qc.decompose()

def doubly_controlled_add_a_mod_N(a: int, N: int, num_qubits: int):
    add_a_gate = quantum_addition_modified(a, num_qubits)
    add_N_gate = quantum_addition_modified(N, num_qubits)
    inverse_add_a_gate = add_a_gate.inverse()
    inverse_add_N_gate = add_N_gate.inverse()
    qft = QFT(num_qubits, do_swaps=False)
    inverse_qft = qft.inverse()

    # Doubly controlled register of size 2
    reg_ctrl = QuantumRegister(2, 'ctrl')
    reg_b = QuantumRegister(num_qubits, 'phi_b')
    reg_aux = QuantumRegister(1, 'aux')
    qc = QuantumCircuit(reg_ctrl, reg_b, reg_aux)

    # Control bit must go first
    qc.append(add_a_gate.control(2), [*reg_ctrl._bits, *reg_b._bits, ])
    qc.append(inverse_add_N_gate, reg_b)
    
    qc.append(inverse_qft, reg_b)
    qc.cx(reg_b[0], reg_aux)
    qc.append(qft, reg_b)

    qc.append(add_N_gate.control(), [*reg_aux._bits, *reg_b._bits])
    qc.append(inverse_add_a_gate.control(2), [*reg_ctrl._bits, *reg_b._bits])

    qc.append(inverse_qft, reg_b)
    qc.x(reg_b[0])
    qc.cx(reg_b[0], reg_aux)
    qc.x(reg_b[0])
    qc.append(qft, reg_b)

    qc.append(add_a_gate.control(2), [*reg_ctrl._bits, *reg_b._bits])

    return qc

def controlled_multiple_a_mod_N(a: int, N: int, x_num_qubit: int, num_qubit: int):

    reg_ctrl = QuantumRegister(1)
    reg_x = QuantumRegister(x_num_qubit)
    reg_b = QuantumRegister(num_qubit)
    reg_aux = QuantumRegister(1)

    qc = QuantumCircuit(reg_ctrl, reg_x, reg_b, reg_aux)

    qft = QFT(num_qubit, do_swaps=False)
    inverse_qft = qft.inverse()

    qc.append(qft, reg_b)
    
    for i in range(reg_x.size):
        new_a = a*2**i % N
        controlled_add_a_mod_N = doubly_controlled_add_a_mod_N(new_a, N, num_qubit)
        qc.append(controlled_add_a_mod_N, [reg_ctrl[0], reg_x[i], *reg_b._bits, reg_aux[0]])

    qc.append(inverse_qft, reg_b)
    
    # display(qc.draw())
    return qc


def controlled_unitary_a(a: int, N: int, x: int, original_a: int, power: int) -> QuantumCircuit:

    num_qubit = ceil(log2(N)) + 1

    # Prepare register x
    binary_x = bin(x)[2:].zfill(num_qubit)

    reg_ctrl = QuantumRegister(1)
    reg_x = QuantumRegister(num_qubit)
    reg_b = QuantumRegister(num_qubit)
    reg_aux = QuantumRegister(1)

    qc = QuantumCircuit(reg_ctrl, reg_x, reg_b, reg_aux)

    # for i in range(len(binary_x)):
    #     if binary_x[i] == '1':
    #         qc.x(reg_x[len(binary_x) - 1- i])

    # Apply CMULT a mod N
    gate = controlled_multiple_a_mod_N(modexp(original_a, power, N), N, reg_x.size, num_qubit)
    qc.append(gate, [reg_ctrl[0], *reg_x._bits, *reg_b._bits, reg_aux[0]])

    # swap register x and b
    for i in range(num_qubit):
        qc.cswap(reg_ctrl[0], reg_x[i], reg_b[i])

    # Apply inverse CMULT a^-1 mod N
    from math import pow, log
    inverse_a = int(modexp(modinv(original_a, N), power, N) % N)
    controlled_multiple_inverse_a_mod_N = controlled_multiple_a_mod_N(inverse_a, N, reg_x.size, num_qubit)
    qc.append(controlled_multiple_inverse_a_mod_N.inverse(), [reg_ctrl[0], *reg_x._bits, *reg_b._bits, reg_aux[0]])

    # display(qc.draw())
    return qc


# print(doubly_controlled_add_a_mod_N(2, 15, 5).draw())
def swap(circuit: QuantumCircuit, register: QuantumRegister) -> None: 
    for i in range(len(register) // 2):
        circuit.swap(register[i], register[len(register) - 1 - i])

def swap_gate(num_qubit) -> QuantumCircuit:
    qc = QuantumCircuit(num_qubit)
    for i in range(num_qubit // 2):
        qc.swap(i, num_qubit - 1- i)
    qc.name = 'swap'

    return qc

def modinv(a: int, m: int) -> int:
    """Returns the modular multiplicative inverse of a with respect to the modulus m."""
    def egcd(a: int, b: int) -> Tuple[int, int, int]:
        print(f'{a}^-1 {b}')
        if a == 0:
            return b, 0, 1
        else:
            g, y, x = egcd(b % a, a)
            return g, x - (b // a) * y, y

    g, x, _ = egcd(a, m)

    print(f"{g} {x}")
    if g != 1:
        raise ValueError("The greatest common divisor of {} and {} is {}, so the "
                            "modular inverse does not exist.".format(a, m, g))
    return x % m


def modexp(x, y, n):
    p = 1
    s = x
    r = y
    while r > 0:
        if r % 2 == 1:
            p = (p * s) % n
        s = (s * s) % n
        r = int(r / 2)
    return p
