from typing import Tuple

def modinv(a: int, m: int) -> int:
    """Returns the modular multiplicative inverse of a with respect to the modulus m."""
    def egcd(a: int, b: int) -> Tuple[int, int, int]:
        print(f'{a}^-1 {b}')
        if a == 0:
            return b, 0, 1
        else:
            g, y, x = egcd(b % a, a)
            return g, x - (b // a) * y, y

    g, x, _ = egcd(a, m)
    if g != 1:
        raise ValueError("The greatest common divisor of {} and {} is {}, so the "
                            "modular inverse does not exist.".format(a, m, g))
    return x % m

modinv(3, 11)