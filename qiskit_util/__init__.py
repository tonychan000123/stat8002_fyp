from qiskit import *
from qiskit.circuit import *

def maj() -> Gate:
    """
    Implementation for MAJ building block for majority block that computes the 
    carry out qubit.
    """

    qreg = QuantumRegister(3)
    qc = QuantumCircuit(qreg)
    qc.cx(2, 1)
    qc.cx(2, 0)
    qc.ccx(0, 1, 2)

    gate: Gate = qc.to_gate()
    gate.name = 'MAJ'
    return gate

def ums() -> Gate:
    """
    Implementation of unmajority-and-sum block which computes the sum of three qubits
    """

    qreg = QuantumRegister(3)
    qc = QuantumCircuit(qreg)
    qc.ccx(0, 1, 2)
    qc.cx(2, 0)
    qc.cx(0, 1)

    gate: Gate = qc.to_gate()
    gate.name = 'UMS'
    return gate

def umj() -> Gate:
    """
    Implementation of unmajority block
    """
    qreg = QuantumRegister(3)
    qc = QuantumCircuit(qreg)
    qc.ccx(0, 1, 2)
    qc.cx(2, 0)
    # ums and umj are similar expcept the last operation
    qc.cx(2, 1)
    gate: Gate = qc.to_gate()
    gate.name = 'umj'
    return gate

def doubling_block(num_qubits: int) -> Gate:
    """
    Implementation of doubling block

      0 --------- a_4
    a_4 --------- a_3
    a_3 --------- a_2
    a_2 --------- a_1
    a_1 --------- a_0
    a_0 --------- 0
    """
    
    qreg = QuantumRegister(num_qubits)
    qc=  QuantumCircuit(qreg)
    
    for qubit in range(num_qubits - 1):
        qc.swap(0, num_qubits - qubit -1)
    
    gate: Gate = qc.to_gate()
    gate.name = 'doubling block'

    return gate
    

if __name__ == '__main__':
    qc: QuantumCircuit = QuantumCircuit(3,1)
    
    qc.append(maj(), [0, 1, 2])
    qc.append(ums(), [0, 1, 2])
    qc.append(doubling_block(3), [0, 1, 2])

    display(qc.draw())