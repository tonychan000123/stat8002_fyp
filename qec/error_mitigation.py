from qiskit import QuantumCircuit, Aer, transpile, assemble
from qiskit.providers.aer.noise import NoiseModel
from qiskit.providers.aer.noise.errors import pauli_error, depolarizing_error
import itertools

def get_noise(p):
    error_meas = pauli_error([('X', p), ('I', 1-p)])
    noise_model = NoiseModel()
    noise_model.add_all_qubit_quantum_error(error_meas, "measure")
    return noise_model

noise_model = get_noise(0.01)

qasm_sim = Aer.get_backend('qasm_simulator')
for state in  ['00','01','10','11']:
    qc = QuantumCircuit(2, 2)
    if state[0] == '1':
        qc.x(1)
    if state[1]=='1':
        qc.x(0)  
    
    qc.measure(qc.qregs[0],qc.cregs[0])
    t_qc = transpile(qc, qasm_sim)
    qobj = assemble(t_qc)
    counts = qasm_sim.run(qobj, noise_model=noise_model, shots=10000).result().get_counts()
    print(state+' becomes', counts)