from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister, Aer, execute
from qiskit.aqua import QuantumInstance
from qiskit.circuit import Gate
from qiskit.circuit.library import QFT
from math import pi, log2, ceil, pow
from qiskit.visualization import plot_histogram
import numpy as np


def quantum_addition(a: int, num_qubits: int) -> QuantumCircuit:
    
    # Find binary representation of a
    binary_a = bin(a)[2:]
    
    # Append zeros to the binary for circuit implementation, extra zero is added to prevent overflow
    binary_a = binary_a.zfill(num_qubits + 1)

    # Construct a quantum circuit such that the upper half is register for variable a and lower half is register b
    reg_a = QuantumRegister(len(binary_a))
    reg_b = QuantumRegister(num_qubits + 1)
    qc = QuantumCircuit(reg_a, reg_b)

    for i in range(0, num_qubits + 1):
        count = 1
        for j in range(i, num_qubits + 1):
            qc.cp(2*pi/2**count, reg_a[j], reg_b[i])
            count += 1
    
    print(qc.draw())

    print(binary_a)
    return qc.decompose()

def quantum_addition_modified(a: int, num_qubits: int) -> QuantumCircuit:
    """
    Modified version of quantum addition to reduce the number of gate it uses
    """
    # Find binary representation of a
    binary_a = bin(a)[2:]
    
    # Append zeros to the binary for circuit implementation, extra zero is added to prevent overflow
    binary_a = binary_a.zfill(num_qubits + 1)

    # Construct a circuit that only contains register b
    reg_b = QuantumRegister(num_qubits + 1)
    qc = QuantumCircuit(reg_b)

    # Declare phase list to store all phases
    phase_list = np.zeros(num_qubits)

    for i in range(0, num_qubits):
        count = 1
        for j in range(i, num_qubits):
            if binary_a[j] == '1':
                phase_list[i] += 2*pi/2**(count)
            count += 1
    
    # The number of gates is now smaller or equal to n+1
    for idx, phase in enumerate(phase_list):
        qc.p(phase, idx)
    
    
    print(qc.draw())

    print(binary_a)
    return qc.decompose()

def doubly_controlled_add_a_mod_N(a: int, N: int, num_qubits: int):
    add_a_gate = quantum_addition_modified(a, num_qubits)
    add_N_gate = quantum_addition_modified(N, num_qubits)
    inverse_add_a_gate = add_a_gate.inverse()
    inverse_add_N_gate = add_N_gate.inverse()

    # Doubly controlled register of size 2
    reg_ctrl = QuantumRegister(2, 'ctrl')
    reg_b = QuantumRegister(num_qubits, 'phi_b')
    reg_aux = QuantumRegister(1, 'aux')
    qc = QuantumCircuit(reg_ctrl, reg_b, reg_aux)

    qc.append(add_a_gate.control(2), [*reg_b._bits, *reg_ctrl._bits])
    qc.append(inverse_add_N_gate, reg_b)
    
    qc.append(QFT(num_qubits).inverse(), reg_b)
    qc.cx(reg_b[reg_b.size - 1], reg_aux)

    qc.append(add_N_gate.control(), [*reg_b._bits, *reg_aux._bits])
    qc.append(QFT(num_qubits).inverse(), reg_b._bits)

    qc.append(add_N_gate.control(), [*reg_b._bits, *reg_aux._bits])
    qc.append(inverse_add_a_gate.control(2), [*reg_b._bits, *reg_ctrl._bits])

    qc.append(QFT(num_qubits).inverse(), reg_b)
    qc.x(reg_b[reg_b.size - 1])
    qc.cx(reg_b[reg_b.size - 1], reg_aux)
    qc.x(reg_b[reg_b.size - 1])
    qc.append(QFT(num_qubits), reg_b)

    qc.append(add_a_gate.control(2), [*reg_b._bits, *reg_ctrl._bits])

    return qc

# print(doubly_controlled_add_a_mod_N(2, 15, 5).draw())

    

    


def _get_angles(a: int, n: int) -> np.ndarray:
    # print(f"a: {a}")
    """Calculates the array of angles to be used in the addition in Fourier Space."""
    s = bin(int(a))[2:].zfill(n + 1)
    angles = np.zeros([n + 1])
    for i in range(0, n + 1):
        for j in range(i, n + 1):
            if s[j] == '1':
                angles[n - i] += pow(2, -(j - i))
        angles[n - i] *= np.pi

    print(">>>>>>>>>>>>>>>>")
    print(angles/np.pi)

    return angles[::-1]

def _phi_add_gate(angles, n: int) -> Gate:
    """Gate that performs addition by a in Fourier Space."""
    circuit = QuantumCircuit(n, name="phi_add")
    for i, angle in enumerate(angles):
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        print(angle)
        print(i)
        circuit.p(angle, i)

    print(circuit.decompose().draw())
    return circuit.to_gate()
    
# Factor 15
N = 15
num_qubits = ceil(log2(N))
reg_a = QuantumRegister(num_qubits)
reg_b = QuantumRegister(num_qubits + 1)
creg_a = ClassicalRegister(num_qubits)
creg_b = ClassicalRegister(num_qubits)

qc = QuantumCircuit(reg_b)

# Case when N = 15
# for idx in range(1, reg_a.size):
#     qc.x(reg_a[idx])

# # Initialize register b to 1
# qc.x(reg_b[reg_b.size - 1]) 

# qc.append(QFT(num_qubits + 1), reg_b._bits)

# qc.append(quantum_addition_modified(N, num_qubits), reg_b._bits)
# qc.barrier()
# qc.append(QFT(num_qubits + 1).inverse(), reg_b._bits)
# qc.measure_all()

# backend = Aer.get_backend('qasm_simulator')
# quantum_instance = QuantumInstance(backend, shots=1024)
# results = execute(qc, backend, shots=2048).result()
# counts = results.get_counts()
# plot_histogram(counts)


# Initialize register b to 1
# qc.x(reg_b[0]) 

qc.append(QFT(num_qubits + 1, do_swaps=True), reg_b)

qc.append(quantum_addition_modified(3, num_qubits), reg_b)
# angles = _get_angles(3, num_qubits)
# qc.append(_phi_add_gate(angles, num_qubits+1), reg_b)
qc.barrier()
qc.append(QFT(num_qubits + 1, do_swaps=True).inverse(), reg_b)
qc.measure_all()

backend = Aer.get_backend('qasm_simulator')
quantum_instance = QuantumInstance(backend, shots=1024)
results = execute(qc, backend, shots=2048).result()
counts = results.get_counts()
print(counts)
plot_histogram(counts)

# print(qc.draw())
# result = shor.run(quantum_instance)

