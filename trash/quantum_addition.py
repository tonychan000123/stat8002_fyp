from qiskit import QuantumCircuit, Aer, execute
from qiskit.circuit.library import QFT
from qiskit.visualization import plot_histogram

import numpy as np
import math

def quantum_addition(a: int, num_qubits: int, qc: QuantumCircuit):
    # Assume num_qubits are same for a and b

    offset = 1
    binary_str = "{0:b}".format(a).zfill(num_qubits)
    print(binary_str)

    for index, char in enumerate(binary_str):
        if char == '1':
            qc.x(index)
    
    for index in range(num_qubits):
        count = 1
        for idx in [i + index for i in range(num_qubits - index)]:
                qc.cp(2*np.pi / float(2**(count)), idx, num_qubits + index)
                count += 1
                
def qft_dagger(n):
    """n-qubit QFTdagger the first n qubits in circ"""
    qc = QuantumCircuit(n)
    # Don't forget the Swaps!
    for qubit in range(n//2):
        qc.swap(qubit, n-qubit-1)
    for j in range(n):
        for m in range(j):
            qc.cp(-np.pi/float(2**(j-m)), m, j)
        qc.h(j)
    qc.name = "QFT†"
    return qc

def qft_rotations(circuit, n):
    if n == 0: # Exit function if circuit is empty
        return circuit
    n -= 1 # Indexes start from 0
    circuit.h(n) # Apply the H-gate to the most significant qubit
    for qubit in range(n):
        # For each less significant qubit, we need to do a
        # smaller-angled controlled rotation: 
        circuit.cp(np.pi/2**(n-qubit), qubit, n)

    return circuit

def swap_registers(circuit, n):
    for qubit in range(n//2):
        circuit.swap(qubit, n-qubit-1)
    return circuit

def qft(circuit, n):
    """QFT on the first n qubits in circuit"""
    qft_rotations(circuit, n)
    swap_registers(circuit, n)
    return circuit.decompose()

num_qubits = 4

qc = QuantumCircuit(num_qubits*2)

qc.x(5)
qc.append(QFT(4, inverse=True), [4,5,6,7])
# for q in range(num_qubits):
#     qc.h(q + num_qubits)
# qc.append(qft(QuantumCircuit(4), 4).decompose(), [4,5,6,7])
# qc.append(qft_rotations(QuantumCircuit(4), 4), [4,5,6,7])

# quantum_addition(5, 4, qc)

# qc.append(qft_dagger(4), [4,5,6,7])
qc.append(QFT(4).inverse(), [4,5,6,7])

qc.barrier()

qc.measure_all()

display(qc.draw('mpl'))


# backend = Aer.get_backend('qasm_simulator')
# job = execute(qc, backend, shots=1, memory=True)
# output = job.result().get_memory()[0]
backend = Aer.get_backend('qasm_simulator')
results = execute(qc, backend, shots=2048).result()
counts = results.get_counts()
plot_histogram(counts)
