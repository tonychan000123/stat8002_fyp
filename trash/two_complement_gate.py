from typing import *
from qiskit import QuantumCircuit, QuantumRegister
from qiskit.circuit import Gate
from qiskit import *
from qiskit.quantum_info.operators import *
from qiskit.visualization import plot_histogram
import numpy as np
from cdkm_adder import CDKMAdder

class OneComplementGate(Gate):
    """
    Implementation of One's complement operator. The first and last gate act as ancilla bits 
    which should not store any information
    """

    def __init__(self, num_qubits: int, label: Optional[str] = None) -> None:
        if num_qubits < 3:
            raise AttributeError("The num_qubits has to be equal or greater than 3.")

        super().__init__('1\'s Comp', num_qubits, [], label = label)

    def _define(self) -> None:
        q = QuantumRegister(self.num_qubits)
        qc = QuantumCircuit(q, name = self.name)

        for qubit in range(self.num_qubits):
            qc.x(qubit)
        
        self.definition = qc
    
    def inverse(self) -> Gate:
        return super().inverse()

    def control(self, num_ctrl_qubits = 1) -> Gate:
        return self.definition.control().to_gate()


class TwoComplementGate(Gate):
    """
    Implementation of two's complement gate for quantum circuit. It's basically a wrapper class
    of one's complement gate and CDKM adder with initialized value equal to |0...01>. The number of 
    qubit should be initialized as 2n + 1 for this gate.

          0 ---------------
        b_1 ---------------
    a_1 = 1 ---------------
        ...
        b_n ---------------
    a_n = 0 ---------------

    """

    def __init__(self, num_qubits: int, params: List = [], label=None):
        super().__init__("2's Comp", num_qubits, params, label=label)

    def _define(self) -> None:
        q = QuantumRegister(self.num_qubits)
        qc = QuantumCircuit(q, name = self.name)

        # The odd index items are the |b1b2...bn>, invert all of them
        qc.append(OneComplementGate(), range(self.num_qubits)[1::2])

        # Set the a_1 to 1 as 
        # 0 ---------
        # b_1 -------
        # a_1 -------
        qc.x(2)
        
        # use CDKM adder to add 1
        qc.append(CDKMAdder(self.num_qubits), range(self.num_qubits))
        self.definition = qc
    
    def control(self) -> Gate:
        return self.definition.control().to_gate()


if __name__ == '__main__':
    
    

    for q1 in range(0,2):
        for q2 in range(0,2):
            qc = QuantumCircuit(10, 10)
            if q1 == 1:
                qc.x(1)

            if q2 == 1:
                qc.x(2)

            print((q1, q2))
            qc.append(TwoComplementGate(4), range(4))
            print(qc.draw())
            
            qc.barrier()
            qc.measure(0,0)
            qc.measure(1,1)
            backend = Aer.get_backend('qasm_simulator')
            job = execute(qc, backend, shots=1, memory=True)
            output = job.result().get_memory()[0]
            print(output)

    