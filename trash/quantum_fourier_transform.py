from qiskit import QuantumCircuit
from math import pi
from qiskit.circuit.library import QFT
from qiskit import *

# For inverse quantum fourier transform, simply use inverse function to reverse all operations
def quantum_fourier_transform(n: int) -> QuantumCircuit:
    qc = QuantumCircuit(n)

    for idx in range(n):
        qc.h(idx)
        
        for q in range(n - idx - 1):
            qc.cp(2*pi/2**(q + 2), (q+1+idx), idx)

    # Swap the qubits as the result is reverted
    for idx in range(n//2):
        qc.swap(idx, (n - 1) - idx)
    
    return qc.decompose()

if __name__ == '__main__':

    qc = QuantumCircuit(4)
    qc.x(1)
    qc.x(2)
    qc.append(quantum_fourier_transform(4), range(4))
    qc.decompose()
    qc.barrier()
    qc.append(QFT(4).inverse(), range(4))
    qc.measure_all()


    backend = Aer.get_backend('qasm_simulator')
    job = execute(qc, backend, shots = 1, memory=True)
    # counts = results.get_counts()
    # plot_histogram(counts)
    output = job.result().get_memory()[0]
    print(output)

    print(qc.draw())

