from qiskit import *
from qiskit.circuit import Gate, ControlledGate
from qiskit_util import maj, ums
from numpy import ndarray
from typing import *
from cdkm_adder import CDKMAdder
from two_complement_gate import TwoComplementGate

def addition_demo():
    # |a_2a_1a_0>
    reg_a = QuantumRegister(3)
    # |b_2b_1b_0>
    reg_b = QuantumRegister(3)

    reg_ancilla = QuantumRegister(3)

    reg_classical = ClassicalRegister(8)
    qc = QuantumCircuit(reg_a, reg_b, reg_ancilla, reg_classical)

    # Set A to 011
    qc.x(0)
    qc.x(1)

    # Set b to 001
    qc.x(3)

    qc.append(CDKMAdder(8), [6, 0, 3, 1, 4, 2, 5, 7])

    qc.barrier()
    
    for i in range(8):
        qc.measure(i,i)

    backend = Aer.get_backend('qasm_simulator')
    job = execute(qc, backend, shots = 1, memory=True)
    # counts = results.get_counts()
    # plot_histogram(counts)
    output = job.result().get_memory()[0]
    print(output)
    print(qc.draw())


# Where all main codes take place
def subtraction_demo():
    # let a = |001> and b = |011>
    # then b - a = 010

    qc = QuantumCircuit(15)
    # index 1,3,5 represents a_0 a_1 a_2
    # index 2,4,6 represent b_0 b_1 b_2
    # the rest are ancilla qubits

    qc.x(1)
    qc.x(2)
    qc.x(4)

    qc.append(TwoComplementGate(7),  [7, 2, 8, 4, 9, 6, 10])
    print(qc.draw())

    qc.barrier()

    qc.


subtraction_demo()
