from qiskit import *
from qiskit.circuit import Gate
from qiskit_util import maj, umj
from numpy import ndarray
from typing import *

class CDKMSubtractor(Gate):
    """
    Implementation of CDKM Subractor. It is the implementation of 2's complement CDKM adder
    """

    def __init__(self, name: str, num_qubits: int, label: Optional[str] = None) -> None:
        super().__init__()
