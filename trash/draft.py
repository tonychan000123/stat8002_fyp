from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister, Aer, execute
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance
from qiskit.visualization import plot_histogram
from qa import quantum_addition_modified, _get_angles, _phi_add_gate

reg = QuantumRegister(5)
qc = QuantumCircuit(reg)

# qc.x(reg[reg.size - 1])

qc.append(QFT(reg.size, do_swaps=False), reg)
angles = _get_angles(4, 4)
qc.append(_phi_add_gate(angles, 5), reg)
qc.append(QFT(reg.size).inverse(), reg)
qc.measure_all()


qc.draw()

backend = Aer.get_backend('qasm_simulator')
quantum_instance = QuantumInstance(backend, shots=1024)
results = execute(qc, backend, shots=2048).result()
counts = results.get_counts()
print(counts)
plot_histogram(counts)