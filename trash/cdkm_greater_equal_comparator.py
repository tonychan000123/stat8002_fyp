from qiskit import *
from qiskit.circuit import Gate, ControlledGate
from qiskit_util import maj, ums
from numpy import ndarray
from typing import *

class CDKMGreaterEqualComparator(Gate):

    def __init__(self, num_qubits, params = [], label=None):
        super().__init__('>=', num_qubits, params, label=label)
    
    def _define(self) -> None:
        q = QuantumRegister(self.num_qubits)
        qc = QuantumCircuit(q, name = self.name)
        arr = [s for s in range(self.num_qubits) if s % 2 == 0][:-1:]
        for q1 in arr:
            
            maj_gate = maj()
            qc.append(maj_gate, [q1, q1+1, q1 + 2])
        
        qc.cx(self.num_qubits-2, self.num_qubits-1)

        for q1 in arr[::-1]:
            
            umj_gate = umj()
            qc.append(umj_gate, [q1, q1 + 1, q1 + 2])

        self.definition = qc

    def inverse(self) -> Gate:
        return super().inverse()

    def control(self, num_ctrl_qubits = 1) -> CCDKMAdder:
        # TODO: Implement the controlled-CDKM adder
        return self.definition.control().to_gate()
    
    def to_matrix(self) -> ndarray:
        pass