from qiskit import *
from qiskit.circuit import Gate, ControlledGate
from qiskit_util import maj, ums
from numpy import ndarray
from typing import *

class CCDKMAdder(ControlledGate):
    """
    Implementation of multi-controlled CDKM Adder, The first and (n-1)th qubit do not store 
    any information and remain in |0> state. The n-th qubit acts as control qubit.
    """

    def __init__(self, num_qubits: int, num_ctrl_qubits: int, label: str = None, ctrl_state = None, _name='mccdkm_adder'):
        super().__init__(_name, num_qubits + num_ctrl_qubits, [], num_ctrl_qubits = num_ctrl_qubits, label = label, ctrl_state = ctrl_state)
        self.base_gate = CDKMAdder(num_qubits)
    
    def inverse(self) -> ControlledGate:
        super().inverse()


class CDKMAdder(Gate):
    """
    Implementation of CDKM Adder. The first and last qubit should not store any information 
    and reamin in |0> state
    """

    def __init__(self, num_qubits: int, label: Optional[str] = None) -> None:
        if num_qubits < 4:
            raise AttributeError("The num_qubits must be equal or greater than 4.")

        super().__init__('CDKM Adder', num_qubits, [], label=label)
        
    def _define(self) -> None:
        q = QuantumRegister(self.num_qubits)
        qc = QuantumCircuit(q, name = self.name)
        arr = [s for s in range(self.num_qubits) if s % 2 == 0][:-1:]
        for q1 in arr:
            
            maj_gate = maj()
            qc.append(maj_gate, [q1, q1+1, q1 + 2])
        
        qc.cx(self.num_qubits-2, self.num_qubits-1)

        for q1 in arr[::-1]:
            
            ums_gate = ums()
            qc.append(ums_gate, [q1, q1 + 1, q1 + 2])

        self.definition = qc

    def inverse(self) -> Gate:
        return super().inverse()

    def control(self, num_ctrl_qubits = 1) -> CCDKMAdder:
        # TODO: Implement the controlled-CDKM adder
        return self.definition.control().to_gate()
    
    def to_matrix(self) -> ndarray:
        pass





if __name__ == '__main__':
    qc = QuantumCircuit(8, 4)
    
    gate = CDKMAdder(8)
    print(gate.definition.draw())

    qc.x(0)
    # qc.x(0)
    qc.x(3)
    qc.x(1)

    qc.append(gate, [6, 0, 3, 1, 4, 2, 5, 7])
    print(qc.draw())

    qc.barrier()
    qc.measure(1, 0)
    qc.measure(3, 1)
    qc.measure(5, 2)
    qc.measure(7, 3)

    backend = Aer.get_backend('qasm_simulator')
    job = execute(qc, backend, shots = 1, memory=True)
    # counts = results.get_counts()
    # plot_histogram(counts)
    output = job.result().get_memory()[0]
    print(output)
